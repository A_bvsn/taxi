from flask import request, session

from src import db

from src.website.models import *
from src.website.lib.wtf_forms import ApplicationForm

error = "Please fill in all forms correctly and then submit order"


def check_validation():
    form = ApplicationForm(request.form)
    if form.validate():
        return add_application()

def add_application():
    name = request.form['name']
    phone = request.form['phone']
    adress = request.form['adress']
    application = Application(name=name, phone=phone, adress=adress)
    db.session.add(application)
    return db.session.commit()

    # form = ApplicationForm(request.form)
    # if form.validate():
    #     name = request.form['name']
    #     phone = request.form['phone']
    #     adress = request.form['adress']
    #
    #     application = Application(name=name, phone=phone, adress=adress)
    #
    #     db.session.add(application)
    #     db.session.commit()
    #
    #     return render_template('success.html')
    # else:
    #     error = "Please fill in all forms correctly and then submit order"
    #     return render_template('main.html', error=error, form=form)
