from flask import request, session

from src.website.lib.wtf_forms import LoginForm


def check_auth(username, password):
    return username == 'admin' and password == 'xxxx'


def user_request():
    if 'user' in session:
        return session['user']


def log_out():
    return session.pop('user', None)


def is_form_validated():
    form = LoginForm(request.form)
    return form.validate()
